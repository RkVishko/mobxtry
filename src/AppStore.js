import { observable, action, computed, decorate, autorun, reaction, when } from 'mobx'

class AppStore {
 todos = []
 filter = ""
 get filteredTodos() {
  const matchedFilter = new RegExp(this.filter, 'i')
  return this.todos.filter(todo => !this.filter || matchedFilter.test(todo))
 }
 onChangeFilter = (e) => {
  this.filter = e.target.value
 }
 onAddTodo = (value) => {
  this.todos.push(value)
 }
}

decorate(AppStore, {
 todos: observable,
 filter: observable,
 onChangeFilter: action,
 filteredTodos: computed
})

const store = window.store = new AppStore()

autorun(() => {
 console.log(store.todos.map(todo => todo), 'autorun')
})

reaction(
 () => store.todos.length,
 length => console.log('reaction1: ', store.todos.map(todo => todo).join(','))
)

reaction(
 () => store.todos.map(todo => todo),
 (values) => console.log('reaction2: ', values.join(','))
)

when(
 () => store.todos.length > 3,
 () => console.log('when')
)

export default store

