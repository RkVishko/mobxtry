import React from 'react';
import { observer } from "mobx-react"
import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`

const App = observer(class App extends React.Component {
 
 addTodo = (e) => {
  const { onAddTodo } = this.props.store
  if (e.which === 13) {
   onAddTodo(e.target.value)
  }
 }
 
 render() {
  const {
   filter,
   filteredTodos,
   onChangeFilter,
  } = this.props.store
  return (
   <Wrapper>
    {filteredTodos.map((todo,index) => (<li key={`${index}-li`}>{todo}</li>))}
    <input onChange={onChangeFilter} value={filter}/>
    <input onKeyPress={this.addTodo}/>
   </Wrapper>
  );
 }
})

export default App;
